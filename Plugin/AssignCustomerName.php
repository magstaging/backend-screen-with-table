<?php

namespace Mbs\BackendScreen\Plugin;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Mbs\BackendScreen\Api\Data\CustomerAnimalExtensionInterfaceFactory;
use Mbs\BackendScreen\Api\Data\CustomerAnimalInterface;
use Mbs\BackendScreen\Model\CustomerNameFinder;

class AssignCustomerName
{
    /**
     * @var CustomerAnimalExtensionInterfaceFactory
     */
    private $customerLogExtension;
    /**
     * @var CustomerNameFinder
     */
    private $customerNameFinder;

    /**
     * AssignCustomerName constructor.
     * @param CustomerAnimalExtensionInterfaceFactory $customerLogExtension
     * @param CustomerNameFinder $customerNameFinder
     */
    public function __construct(
        CustomerAnimalExtensionInterfaceFactory $customerLogExtension,
        CustomerNameFinder $customerNameFinder
    ) {
        $this->customerLogExtension = $customerLogExtension;
        $this->customerNameFinder = $customerNameFinder;
    }

    /**
     * @param CustomerRepositoryInterface $subject
     * @param CustomerAnimalInterface $customerAnimal
     */
    public function afterGet(
        CustomerRepositoryInterface $subject,
        CustomerAnimalInterface $customerAnimal
    ) {
        $extensionAttributes = $this->getExtensionAttribute($customerAnimal);

        $extensionAttributes->setData(
            'customer_name',
            $this->customerNameFinder->getCustomerName($customerAnimal)
        );
    }

    /**
     * @param CustomerAnimalInterface $customerLog
     * @return \Magento\Framework\Api\ExtensionAttributesInterface|null
     */
    private function getExtensionAttribute(CustomerAnimalInterface $customerLog)
    {
        $extensionAttributes = $customerLog->getExtensionAttributes();
        if ($extensionAttributes === null) {
            $extensionAttributes = $this->customerLogExtension->create();
        }

        return $extensionAttributes;
    }
}
