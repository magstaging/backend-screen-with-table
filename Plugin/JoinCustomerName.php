<?php

namespace Mbs\BackendScreen\Plugin;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory;
use Mbs\BackendScreen\Model\CustomerNameLinkProvider;

class JoinCustomerName
{
    /**
     * @var CustomerNameLinkProvider
     */
    private $customerNameLinkProvider;

    /**
     * JoinCustomerName constructor.
     * @param CustomerNameLinkProvider $customerNameLinkProvider
     */
    public function __construct(
        CustomerNameLinkProvider $customerNameLinkProvider
    ) {
        $this->customerNameLinkProvider = $customerNameLinkProvider;
    }

    public function afterGetReport(
        CollectionFactory $subject,
        $collection,
        $requestName
    ) {
        if ($requestName == 'customeranimal_listing_data_source') {
            try {
                $this->customerNameLinkProvider->assignCustomerNameToCollection($collection);
            } catch (NoSuchEntityException $e) {
            }
        }
        return $collection;
    }
}
