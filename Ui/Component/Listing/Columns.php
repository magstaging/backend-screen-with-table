<?php

namespace Mbs\BackendScreen\Ui\Component\Listing;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Mbs\BackendScreen\Ui\Component\Listing\Column\CustomColumnCreator;

class Columns extends \Magento\Ui\Component\Listing\Columns
{
    /**
     * @var Column\CustomColumnCreator
     */
    private $columnCreator;

    public function __construct(
        ContextInterface $context,
        CustomColumnCreator $columnCreator,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $components, $data);
        $this->columnCreator = $columnCreator;
    }

    public function prepare()
    {
        if (isset($this->components['customer_name'])) {
            $customerNameColumn = $this->components['customer_name'];

            $column = $this->columnCreator->addColumnFromExistingColumn(
                $customerNameColumn,
                'customer_firstname',
                'Customer Firstname',
                11
            );
            $this->addComponent('customer_firstname', $column);

            $column = $this->columnCreator->addColumnFromExistingColumn(
                $customerNameColumn,
                'customer_lastname',
                'Customer Lastname',
                12
            );
            $this->addComponent('customer_lastname', $column);
        }

        parent::prepare();
    }
}
