<?php


namespace Mbs\BackendScreen\Ui\Component\Animal;


use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;
use Magento\Ui\DataProvider\ModifierPoolDataProvider;
use Mbs\BackendScreen\Model\CustomerAnimal;
use Mbs\BackendScreen\Model\ResourceModel\CustomerAnimal\CollectionFactory;

class DataProvider extends ModifierPoolDataProvider
{
    /**
     * @var \Mbs\BackendScreen\Model\ResourceModel\CustomerAnimal\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     * @param PoolInterface|null $pool
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = [],
        PoolInterface $pool = null
    ) {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var CustomerAnimal $customerAnimal */
        foreach ($items as $customerAnimal) {
            $this->loadedData[$customerAnimal->getId()] = $customerAnimal->getData();
        }

        $data = $this->dataPersistor->get('customer_animal');
        if (!empty($data)) {
            $customerAnimal = $this->collection->getNewEmptyItem();
            $customerAnimal->setData($data);
            $this->loadedData[$customerAnimal->getId()] = $customerAnimal->getData();
            $this->dataPersistor->clear('customer_animal');
        }

        return $this->loadedData;
    }
}
