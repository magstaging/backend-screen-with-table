<?php

namespace Mbs\BackendScreen\Block\Adminhtml\CustomerAnimal\Edit;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Mbs\BackendScreen\Model\CustomerAnimalFactory;

class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    private CustomerAnimalFactory $customerAnimalFactory;

    /**
     * @param Context $context
     * @param CustomerAnimalFactory $customerAnimalFactory
     */
    public function __construct(
        Context $context,
        CustomerAnimalFactory $customerAnimalFactory
    ) {
        $this->context = $context;
        $this->customerAnimalFactory = $customerAnimalFactory;
    }

    /**
     * Return Schedule ID
     *
     * @return int|null
     */
    public function getAnimalId()
    {
        try {
            $animal = $this->customerAnimalFactory->create();
            return $animal->load($this->context->getRequest()->getParam('id'))->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
