<?php

namespace Mbs\BackendScreen\Block\Adminhtml\CustomerAnimal;

use Magento\Customer\Controller\RegistryConstants;

class AssignAnimals extends \Magento\Backend\Block\Template
{
    /**
     * Block template
     *
     * @var string
     */
    protected $_template = 'Mbs_BackendScreen::customer/animal/assign_animals.phtml';

    /**
     * @var \Mbs\BackendScreen\Block\Adminhtml\CustomerAnimal\Tab\Animal
     */
    protected $blockGrid;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $jsonEncoder;
    private \Mbs\BackendScreen\Model\CustomerAnimalReader $customerAnimalReader;

    /**
     * AssignProducts constructor.
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Mbs\BackendScreen\Model\CustomerAnimalReader $customerAnimalReader
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Mbs\BackendScreen\Model\CustomerAnimalReader $customerAnimalReader,
        array $data = []
    ) {
        $this->registry = $registry;
        $this->jsonEncoder = $jsonEncoder;
        parent::__construct($context, $data);
        $this->customerAnimalReader = $customerAnimalReader;
    }

    /**
     * Retrieve instance of grid block
     *
     * @return \Magento\Framework\View\Element\BlockInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBlockGrid()
    {
        if (null === $this->blockGrid) {
            $this->blockGrid = $this->getLayout()->createBlock(
                \Mbs\BackendScreen\Block\Adminhtml\CustomerAnimal\Tab\Animal::class,
                'customer.animal.grid'
            );
        }
        return $this->blockGrid;
    }

    /**
     * Return HTML of grid block
     *
     * @return string
     */
    public function getGridHtml()
    {
        return $this->getBlockGrid()->toHtml();
    }

    /**
     * @return string
     */
    public function getAnimalsJson()
    {
        return $this->customerAnimalReader->getAllAnimalsInJson($this->getCustomer());
    }

    /**
     * @return array|null
     */
    public function getCustomer()
    {
        $customerId = $this->registry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
        return $customerId;
    }
}
