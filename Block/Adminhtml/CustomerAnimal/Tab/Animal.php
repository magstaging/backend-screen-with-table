<?php

namespace Mbs\BackendScreen\Block\Adminhtml\CustomerAnimal\Tab;

use Magento\Backend\Block\Widget\Grid;
use Magento\Backend\Block\Widget\Grid\Column;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Customer\Controller\RegistryConstants;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObjectFactory;
use Magento\Framework\Serialize\SerializerInterface;

class Animal extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var Status
     */
    private $status;

    /**
     * @var Visibility
     */
    private $visibility;
    private \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository;
    private \Mbs\BackendScreen\Model\CustomerAnimalFactory $customerAnimalFactory;
    private \Magento\Framework\Data\CollectionFactory $collectionFactory;
    private \Magento\Framework\Data\ObjectFactory $objectFactory;
    /**
     * @var DataObjectFactory
     */
    private DataObjectFactory $dataObjectFactory;
    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;
    private \Mbs\BackendScreen\Model\CustomerAnimalReader $customerAnimalReader;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Mbs\BackendScreen\Model\CustomerAnimalFactory $customerAnimalFactory
     * @param \Magento\Framework\Data\CollectionFactory $collectionFactory
     * @param DataObjectFactory $dataObjectFactory
     * @param SerializerInterface $serializer
     * @param \Mbs\BackendScreen\Model\CustomerAnimalReader $customerAnimalReader
     * @param array $data
     * @param Visibility|null $visibility
     * @param Status|null $status
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Registry $coreRegistry,
        \Mbs\BackendScreen\Model\CustomerAnimalReader $customerAnimalReader,
        array $data = [],
        Visibility $visibility = null,
        Status $status = null
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->visibility = $visibility ?: ObjectManager::getInstance()->get(Visibility::class);
        $this->status = $status ?: ObjectManager::getInstance()->get(Status::class);
        parent::__construct($context, $backendHelper, $data);
        $this->customerAnimalReader = $customerAnimalReader;
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('customer_animals');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
    }

    /**
     * @return array|null
     */
    public function getCustomer()
    {
        $customerId = $this->_coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
        return $customerId;
    }

    /**
     * @param Column $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        return $this;
    }

    /**
     * @return Grid
     */
    protected function _prepareCollection()
    {
        $collection = $this->customerAnimalReader->getAllAnimals($this->getCustomer());

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return Extended
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'animal_name',
            [
                'header' => __('Animal'),
                'type' => 'text',
                'index' => 'animal_name',
                'editable' => false
            ]
        );

        $this->addColumn(
            'animal_number',
            [
                'header' => __('Animal Number'),
                'type' => 'number',
                'index' => 'animal_number',
                'editable' => true
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('customer/*/grid', ['_current' => true]);
    }
}
