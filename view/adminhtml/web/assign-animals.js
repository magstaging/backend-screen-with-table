/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/* global $, $H */

define([
    'mage/adminhtml/grid'
], function () {
    'use strict';

    return function (config) {
        var animalsInfo = config.animalsInfo,
            gridJsObject = window[config.gridJsObjectName],
            tabIndex = 1000;

        $('in_customer_animals').value = Object.toJSON(animalsInfo);

        /**
         * Register Category Product
         *
         * @param {Object} grid
         * @param {Object} element
         * @param {Boolean} checked
         */
        function registerCustomerAnimal(grid, element, checked) {
            $('in_customer_animals').value = Object.toJSON(animalsInfo);
        }

        /**
         * Change product amount
         *
         * @param {String} event
         */
        function amountChange(event) {
            var element = Event.element(event);

            if (element) {
                for (var i = 0; i < animalsInfo.length; i++) {
                    if (animalsInfo[i].species === element.animalObject.species) {
                        animalsInfo[i].amount = element.value;
                        break;
                    }
                }
                $('in_customer_animals').value = Object.toJSON(animalsInfo);
            }
        }

        /**
         * Initialize category product row
         *
         * @param {Object} grid
         * @param {String} row
         * @param {String} key
         */
        function customerAnimalRowInit(grid, row, key) {
            var amount = $(row).getElementsByClassName('input-text')[0];

            if (amount) {
                amount.animalObject = animalsInfo[key];
                amount.tabIndex = tabIndex++;
                Event.observe(amount, 'keyup', amountChange);
            }
        }

        gridJsObject.initRowCallback = customerAnimalRowInit;
        gridJsObject.checkboxCheckCallback = registerCustomerAnimal;

        if (gridJsObject.rows) {
            gridJsObject.rows.each(function (row, key) {
                customerAnimalRowInit(gridJsObject, row, key);
            });
        }
    };
});
