<?php

namespace Mbs\BackendScreen\Api;

interface CustomerAnimalRepositoryInterface
{
    /**
     * @param string $email
     * @return \Mbs\BackendScreen\Api\Data\CustomerAnimalInterface[]
     */
    public function getListAnimalByCustomerEmail($email);
}
