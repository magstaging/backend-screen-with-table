<?php

namespace Mbs\BackendScreen\Model;

use Magento\Framework\Model\AbstractModel;
use Mbs\BackendScreen\Api\Data\CustomerDataInterface;

class CustomerData extends AbstractModel implements CustomerDataInterface
{
    /**
     * @inheritDoc
     */
    public function getCustomerName()
    {
        return $this->_getData('customer_name');
    }

    /**
     * @inheritDoc
     */
    public function setCustomerName($name)
    {
        $this->setData('customer_name', $name);
    }
}