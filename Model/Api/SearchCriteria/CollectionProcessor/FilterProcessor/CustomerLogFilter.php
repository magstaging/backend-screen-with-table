<?php

namespace Mbs\BackendScreen\Model\Api\SearchCriteria\CollectionProcessor\FilterProcessor;

use Magento\Framework\Api\Filter;
use Magento\Framework\Api\SearchCriteria\CollectionProcessor\FilterProcessor\CustomFilterInterface;
use Magento\Framework\Data\Collection\AbstractDb;

class CustomerLogFilter implements CustomFilterInterface
{
    /**
     * @inheritDoc
     */
    public function apply(Filter $filter, AbstractDb $collection)
    {
        $join[] = 'customer.entity_id=main_table.customer_id';
        $join[] = $collection->getSelect()->getConnection()->quoteInto('customer.email=?', $filter->getValue());
        $collection->join(
            ['customer' => $collection->getTable('customer_entity')],
            implode(' and ', $join),
            ''
        );

        return true;
    }
}
