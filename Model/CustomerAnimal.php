<?php

namespace Mbs\BackendScreen\Model;

use Mbs\BackendScreen\Api\Data\CustomerAnimalInterface;

class CustomerAnimal extends \Magento\Framework\Model\AbstractExtensibleModel
    implements \Mbs\BackendScreen\Api\Data\CustomerAnimalInterface
{
    protected function _construct()
    {
        $this->_init(ResourceModel\CustomerAnimal::class);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerId()
    {
        return $this->_getData('customer_id');
    }

    /**
     * @inheritDoc
     */
    public function setCustomerId($id)
    {
        $this->setData('customer_id', $id);
    }

    /**
     * @inheritDoc
     */
    public function getAnimalName()
    {
        return $this->_getData('animal_name');
    }

    /**
     * @inheritDoc
     */
    public function setAnimalName($name)
    {
        $this->setData('animal_name', $name);
    }

    /**
     * @inheritDoc
     */
    public function getAnimalNumber()
    {
        return $this->_getData('animal_number');
    }

    /**
     * @inheritDoc
     */
    public function setAnimalNumber($number)
    {
        $this->setData('animal_number', $number);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt()
    {
        return $this->_getData('created_at');
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt($date)
    {
        $this->setData('created_at', $date);
    }

    /**
     * @inheritDoc
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * @inheritDoc
     */
    public function setExtensionAttributes(\Mbs\BackendScreen\Api\Data\CustomerAnimalExtensionInterface $extensionAttributes)
    {
        $this->_setExtensionAttributes($extensionAttributes);
    }
}
