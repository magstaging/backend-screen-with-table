<?php

namespace Mbs\BackendScreen\Model;

use Magento\Framework\DataObjectFactory;
use Magento\Framework\Serialize\SerializerInterface;

class CustomerAnimalReader
{
    private static $dataType = 'json'; // can be either json or array

    private \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository;

    /**
     * @var CustomerAnimalReader\CustomerReaderWithJson
     */
    private CustomerAnimalReader\CustomerReaderWithJson $customerReaderWithJson;
    /**
     * @var CustomerAnimalReader\CustomerReaderWithArray
     */
    private CustomerAnimalReader\CustomerReaderWithArray $customerReaderWithArray;
    private \Magento\Framework\Data\CollectionFactory $collectionFactory;
    /**
     * @var DataObjectFactory
     */
    private DataObjectFactory $dataObjectFactory;
    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * CustomerAnimalReader constructor.
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param CustomerAnimalReader\CustomerReaderWithJson $customerReaderWithJson
     * @param CustomerAnimalReader\CustomerReaderWithArray $customerReaderWithArray
     * @param \Magento\Framework\Data\CollectionFactory $collectionFactory
     * @param DataObjectFactory $dataObjectFactory
     * @param SerializerInterface $serializer
     */
    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Mbs\BackendScreen\Model\CustomerAnimalReader\CustomerReaderWithJson $customerReaderWithJson,
        \Mbs\BackendScreen\Model\CustomerAnimalReader\CustomerReaderWithArray $customerReaderWithArray,
        \Magento\Framework\Data\CollectionFactory $collectionFactory,
        DataObjectFactory $dataObjectFactory,
        SerializerInterface $serializer
    ) {
        $this->customerRepository = $customerRepository;
        $this->customerReaderWithJson = $customerReaderWithJson;
        $this->customerReaderWithArray = $customerReaderWithArray;
        $this->collectionFactory = $collectionFactory;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->serializer = $serializer;
    }

    public function getAllAnimals($customerId)
    {
        if (self::$dataType == 'json') {
            $collection = $this->customerReaderWithJson->getAllAnimalsFromJson($customerId);
        }

        if (self::$dataType == 'array') {
            $collection = $this->customerReaderWithArray->getAllAnimalsFromArray($customerId);
        }

        return $this->buildAllAnimalCollectionWithCustomerData($collection);
    }

    private function buildAllAnimalCollectionWithCustomerData($collection)
    {
        $collectionResult = $this->collectionFactory->create();
        $animals = ['dog', 'cat', 'horse', 'parrot', 'mouse', 'pig', 'chicken'];
        sort($animals);

        foreach ($animals as $animal) {
            $animalObject = $this->dataObjectFactory->create();
            $animalObject->setAnimalName($animal);
            $item = $collection->getItemByColumnValue('animal_name', ucfirst($animal));
            if ($item) {
                $animalObject->setAnimalNumber($item->getData('animal_number'));
            } else {
                $animalObject->setAnimalNumber(0);
            }

            $collectionResult->addItem($animalObject);
        }

        return $collectionResult;
    }

    public function getAllAnimalsInJson(int $customerId)
    {
        $collection = $this->getAllAnimals($customerId);

        $result = [];
        foreach ($collection as $item) {
            $result[] = ['species' => $item->getData('animal_name'), 'amount' => $item->getData('animal_number')];
        }

        return $this->serializer->serialize($result);
    }
}
