<?php

namespace Mbs\BackendScreen\Model\ResourceModel\CustomerAnimal;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Mbs\BackendScreen\Model\CustomerAnimal;
use Mbs\BackendScreen\Model\ResourceModel\CustomerAnimal as CustomerAnimalResource;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(CustomerAnimal::class, CustomerAnimalResource::class);
    }

}
