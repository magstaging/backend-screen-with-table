<?php

namespace Mbs\BackendScreen\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class CustomerAnimal extends AbstractDb
{
    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init('mbs_customer_animal', 'id');
    }
}
