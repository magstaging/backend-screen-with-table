<?php

namespace Mbs\BackendScreen\Model;

use Magento\Framework\Exception\NoSuchEntityException;

class CustomerAnimalRepository implements \Mbs\BackendScreen\Api\CustomerAnimalRepositoryInterface
{
    /**
     * @var ResourceModel\CustomerAnimal\CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface
     */
    private $collectionProcessor;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaInterface
     */
    private $criteria;
    /**
     * @var CustomerNameLinkProvider
     */
    private $customerNameLinkProvider;

    public function __construct(
        \Mbs\BackendScreen\Model\ResourceModel\CustomerAnimal\CollectionFactory $collectionFactory,
        \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface $collectionProcessor,
        \Magento\Framework\Api\SearchCriteriaInterface $criteria,
        \Mbs\BackendScreen\Model\CustomerNameLinkProvider $customerNameLinkProvider
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->criteria = $criteria;
        $this->customerNameLinkProvider = $customerNameLinkProvider;
    }

    /**
     * @inheritDoc
     * @throws NoSuchEntityException
     */
    public function getLastLogByCustomerEmail($email)
    {
        /** @var \Mbs\BackendScreen\Model\ResourceModel\CustomerAnimal\Collection $collection */
        $collection = $this->initialiseCustomerNameWithEmail($email);
        $collection->setOrder('created_at', \Magento\Framework\Data\Collection::SORT_ORDER_DESC);
        $collection->setPageSize(1);

        if ($collection->count()==0) {
            throw new NoSuchEntityException(__('The Customer email does not have customer logs.', $email));
        }

        $this->customerNameLinkProvider->assignCustomerNameToCollection($collection);

        return $collection->getFirstItem();
    }

    /**
     * @inheritDoc
     * @throws NoSuchEntityException
     */
    public function getListAnimalByCustomerEmail($email)
    {
        $collection = $this->initialiseCustomerNameWithEmail($email);

        if ($collection->count()==0) {
            throw new NoSuchEntityException(__('The Customer email does not have animal.', $email));
        }

        return $collection->getItems();
    }

    /**
     * @param $email
     * @return ResourceModel\CustomerAnimal\Collection
     */
    private function initialiseCustomerNameWithEmail($email): ResourceModel\CustomerAnimal\Collection
    {
        /** @var \Mbs\BackendScreen\Model\ResourceModel\CustomerAnimal\Collection $collection */
        $collection = $this->collectionFactory->create();
        $join[] = 'customer.entity_id=main_table.customer_id';
        $join[] = $collection->getSelect()->getConnection()->quoteInto('customer.email=?', $email);
        $collection->join(
            ['customer' => $collection->getTable('customer_entity')],
            implode(' and ', $join),
            ''
        );

        return $collection;
    }
}
