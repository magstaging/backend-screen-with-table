<?php

namespace Mbs\BackendScreen\Model\CustomerAnimalReader;

use Magento\Framework\Data\Collection;
use Magento\Framework\DataObjectFactory;

class CustomerReaderWithArray
{
    private \Magento\Framework\Data\CollectionFactory $collectionFactory;
    /**
     * @var DataObjectFactory
     */
    private DataObjectFactory $dataObjectFactory;

    /**
     * CustomerReaderWithArray constructor.
     * @param \Magento\Framework\Data\CollectionFactory $collectionFactory
     * @param DataObjectFactory $dataObjectFactory
     */
    public function __construct(
        \Magento\Framework\Data\CollectionFactory $collectionFactory,
        DataObjectFactory $dataObjectFactory
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->dataObjectFactory = $dataObjectFactory;
    }

    /**
     * @param int $customerId
     * @return Collection
     * @throws \Exception
     */
    public function getAllAnimalsFromArray(int $customerId): Collection
    {
        $arrayAnimalCustomerData = $this->getCustomerAnimalsInArrayFormat($customerId);

        $collection = $this->collectionFactory->create();
        foreach ($arrayAnimalCustomerData as $item) {
            $animalObject = $this->dataObjectFactory->create();
            $animalObject->setAnimalName($item);
            $animalObject->setAnimalNumber(1);

            $collection->addItem($animalObject);
        }

        return $collection;
    }

    // this function in real implementation will get the animal information for a customer record
    // and this particular function will return a record that is json formatted
    private function getCustomerAnimalsInArrayFormat(int $customerId)
    {
        $json = ['Dog', 'Chicken'];
        return $json;
    }
}
