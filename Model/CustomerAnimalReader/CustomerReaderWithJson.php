<?php

namespace Mbs\BackendScreen\Model\CustomerAnimalReader;

use Magento\Framework\Data\Collection;
use Magento\Framework\DataObjectFactory;
use Magento\Framework\Serialize\SerializerInterface;

class CustomerReaderWithJson
{
    private \Magento\Framework\Data\CollectionFactory $collectionFactory;
    /**
     * @var DataObjectFactory
     */
    private DataObjectFactory $dataObjectFactory;
    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * CustomerReaderWithJson constructor.
     * @param \Magento\Framework\Data\CollectionFactory $collectionFactory
     * @param DataObjectFactory $dataObjectFactory
     * @param SerializerInterface $serializer
     */
    public function __construct(
        \Magento\Framework\Data\CollectionFactory $collectionFactory,
        DataObjectFactory $dataObjectFactory,
        SerializerInterface $serializer
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->serializer = $serializer;
    }

    /**
     * @param $customerId
     * @return Collection
     * @throws \Exception
     */
    public function getAllAnimalsFromJson($customerId): Collection
    {
        $jsonAnimalCustomerData = $this->getCustomerAnimalsInJsonFormat($customerId);
        $arrayToParse = $this->serializer->unserialize($jsonAnimalCustomerData);

        $collection = $this->collectionFactory->create();
        foreach ($arrayToParse as $item) {
            $animalObject = $this->dataObjectFactory->create();
            $animalObject->setAnimalName($item['species']);
            $animalObject->setAnimalNumber($item['amount']);

            $collection->addItem($animalObject);
        }

        return $collection;
    }

    // this function in real implementation will get the animal information for a customer record
    // and this particular function will return a record that is json formatted
    /**
     * @param int $customerId
     * @return string
     */
    private function getCustomerAnimalsInJsonFormat(int $customerId): string
    {
        $json = '[{"species":"Horse","amount":"4"},{"species":"Cat","amount":"2"}]';
        return $json;
    }
}
