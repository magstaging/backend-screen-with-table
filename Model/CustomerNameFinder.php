<?php

namespace Mbs\BackendScreen\Model;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Mbs\BackendScreen\Api\Data\CustomerAnimalInterface;

class CustomerNameFinder
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * CustomerNameFinder constructor.
     * @param CustomerRepositoryInterface $customerRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder
    ) {
        $this->customerRepository = $customerRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
    }

    /**
     * @param CustomerAnimalInterface $customerAnimal
     * @return string
     */
    public function getCustomerName(CustomerAnimalInterface $customerAnimal)
    {
        try {
            $customer = $this->customerRepository->getById($customerAnimal->getCustomerId());
            $name = sprintf('%s %s', $customer->getFirstname(), $customer->getLastname());
        } catch (NoSuchEntityException $e) {
            $name = 'removed customer';
        } catch (LocalizedException $e) {
            $name = 'removed customer';
        }

        return $name;
    }
}
