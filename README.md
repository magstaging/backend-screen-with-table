# README #

Add a backend screen that shows customer animals data. This screen is only for study purpose and does not
create useful data.

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/BackendScreen when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

Go to the customer screen and edit a customer, a tab section will appear and it is possible to change the animals for the cust
omer entity. The data are not saved but the controller receives the animals info and therefore we could also save this info if the feature was a real world feature
