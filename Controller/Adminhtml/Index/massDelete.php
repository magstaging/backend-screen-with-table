<?php

namespace Mbs\BackendScreen\Controller\Adminhtml\Index;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;

class massDelete extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Mbs_BackendScreen::customer_animal';

    /**
     * @inheritDoc
     */
    public function execute()
    {
        $postData = $this->getRequest()->getParams();

        $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been deleted.', 1));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('*/*/');
    }
}
