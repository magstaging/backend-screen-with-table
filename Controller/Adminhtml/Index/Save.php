<?php

namespace Mbs\BackendScreen\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Mbs\BackendScreen\Model\CustomerAnimalFactory;

class Save extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Mbs_BackendScreen::customer_animal';

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    private CustomerAnimalFactory $customerAnimalFactory;

    /**
     * Save constructor.
     * @param Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param CustomerAnimalFactory $customerAnimalFactory
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        CustomerAnimalFactory $customerAnimalFactory
    ) {
        parent::__construct($context);
        $this->customerAnimalFactory = $customerAnimalFactory;
        $this->dataPersistor = $dataPersistor;
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->customerAnimalFactory->create();

            $id = $this->getRequest()->getParam('id');
            if ($id) {
                try {
                    $model->load($id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This animal no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            } else {
                unset($data['id']);
            }

            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the animal.'));
                $this->dataPersistor->clear('customer_animal');
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the schedule.'));
            }

            $this->dataPersistor->set('customer_animal', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
